package view;

import controller.boardController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class LoadBoardView {

    private static final String MAIN_SCREEN_FXML = "/view/MainScreen.fxml";
    private Stage window;

    public LoadBoardView(Stage window) {
        this.window = window;
    }

    public void setMainScreen() {
        //this.window.setScene(this.getScene(getClass().getResource(MAIN_SCREEN_FXML)));

        boardController bc = new boardController(window);
        URL resource = getClass().getResource(MAIN_SCREEN_FXML);

        try {
            FXMLLoader loader = new FXMLLoader(resource);
            loader.setController(bc);
            Scene scene = new Scene(loader.load(), 400, 750);
            scene.setOnKeyPressed(e -> bc.moveTiles(e.getCode()));
            this.window.setScene(scene);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
