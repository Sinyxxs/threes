import javafx.application.Application;
import javafx.stage.Stage;
import view.LoadBoardView;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage window) {
        LoadBoardView lbv = new LoadBoardView(window);
        lbv.setMainScreen();

        window.setTitle("Test your might!");
        window.show();
    }
}
