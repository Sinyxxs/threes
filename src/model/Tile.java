package model;

public class Tile {
    private Integer value;

    public Tile() {}

    public Tile(Integer value) {
        this.value = value;
    }

    public void merge(Tile tile) {
        value = value + tile.getValue();
    }

    public Integer getValue() { return value; }

    public void setValue(Integer value) {
        this.value = value;
    }
}
