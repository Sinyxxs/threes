package model;

import java.util.Random;

public class Utils {
    public int rng(int max, int min) {
        Random random = new Random();
        return random.nextInt(max + 1 - min) + min;
    }
}
