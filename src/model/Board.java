package model;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Board {
    private static final String INPUT_FILE = "src/resources/input.txt";
    private Utils utils = new Utils();
    private int moveCounter = 0;
    private boolean ableToMove = true;
    private boolean gameOver = false;
    private Tile[][] tiles = new Tile[4][4];

    public Board(){
        for (int x = 0; x < tiles[0].length; x++) {
            for (int y = 0; y < tiles.length; y++) {
                tiles[x][y] = new Tile();
            }
        }

        if(!fileExists()){
            for (int i = 0; i < 7; i++) {
                generateStarterTiles();
            }
        }
    }

    private void generateStarterTiles() {
        int randomRow = utils.rng(3, 0);
        int randomCol = utils.rng(3, 0);
        int randomValue = utils.rng(3, 1);

        if (tiles[randomRow][randomCol].getValue() == null) {
            tiles[randomRow][randomCol] = new Tile(randomValue);
        } else {
            //Speciaal voor Burak Imre
            generateStarterTiles();
        }
    }

    // File functions
    private boolean fileExists() {
        File f = new File(INPUT_FILE);
        boolean completed = false;

        if(f.exists()){
            completed = readFile(f);
        }
        else{
            completed = false;
        }

        return completed;
    }

    private boolean readFile(File file){
        try {
            List<String> loadList = new ArrayList<>();
            Scanner fileReader = new Scanner(file);
            List<List<String>> listOfLoadLists = new ArrayList<>();

            while(fileReader.hasNextLine()){
                loadList.add((fileReader.nextLine()));
                listOfLoadLists.add(loadList);
            }

            for(List<String> list : listOfLoadLists){
                boolean allCorrect = true;

                for(String totalValues : list){
                    String[] values = totalValues.split("\\s+");

                    int y = Integer.parseInt(values[0]);
                    int x = Integer.parseInt(values[1]);
                    int value = Integer.parseInt(values[2]);
                    if(values != null){
                        if(!checkValues(y, x, value)){
                            allCorrect = false;
                        }
                    } else
                    {
                        return false;
                    }

                }
                if(allCorrect){
                    for(String totalValues : list){
                        String[] values = totalValues.split("\\s+");

                        int y = Integer.parseInt(values[0]);
                        int x = Integer.parseInt(values[1]);
                        int value = Integer.parseInt(values[2]);

                        tiles[x][y] = new Tile(value);
                    }
                    return true;
                }
                else{
                    return false;
                }
            }
            return false;
        }
        catch(Exception e){
            return false;
        }
    }

    private boolean checkValues(int y, int x, int value){
        return y < 0 || y > 3 || x < 0 || x > 3 || value == 1 || value == 2 || value % 3 == 0;
    }

    // Move functions
    public void moveRight() {
        boolean canPlace = false;

        for (int x = 3; x >= 0; x--) {
            for (int y = 3; y >= 0; y--) {
                if (tiles[x][y].getValue() != null) {
                    if (!goingOutOfBounds("right", x, y)) {
                        if(gameOverChecker())
                        {
                            gameOver = false;
                            if (mergable(x, y, 1, 0)) {
                                tiles[x + 1][y].merge(tiles[x][y]);
                                tiles[x][y].setValue(null);
                                canPlace = true;
                            }
                            if (tiles[x + 1][y].getValue() == null) {
                                tiles[x + 1][y].setValue(tiles[x][y].getValue());
                                tiles[x][y].setValue(null);
                                canPlace = true;
                            }
                        }
                        else {
                            gameOver = true;
                        }
                    }
                }
            }
        }
        if(canPlace){
            createNewTile(0, "right");
            moveCounter++;
            ableToMove = true;
        }
        else {
            ableToMove = false;
        }
    }

    public void moveLeft() {
        boolean canPlace = false;

        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                if (tiles[x][y].getValue() != null) {
                    if (!goingOutOfBounds("left", x, y)) {
                        if(gameOverChecker()){
                            if (mergable(x, y, -1, 0)) {
                                tiles[x - 1][y].merge(tiles[x][y]);
                                tiles[x][y].setValue(null);
                                canPlace = true;
                            } else if (tiles[x - 1][y].getValue() == null) {
                                tiles[x - 1][y].setValue(tiles[x][y].getValue());
                                tiles[x][y].setValue(null);
                                canPlace = true;
                            }
                        }
                        else{
                            gameOver = true;
                        }
                    }
                }
            }
        }
        if(canPlace){
            createNewTile(3, "left");
            moveCounter++;
            ableToMove = true;
        }
        else {
            ableToMove = false;
        }
    }

    public void moveUp() {
        boolean canPlace = false;

        for (int x = 3; x >= 0; x--) {
            for (int y = 0; y < 4; y++) {
                if (tiles[x][y].getValue() != null) {
                    if (!goingOutOfBounds("up", x, y)) {
                        if(gameOverChecker()){
                            if (mergable(x, y, 0, -1)) {
                                tiles[x][y - 1].merge(tiles[x][y]);
                                tiles[x][y].setValue(null);
                                canPlace = true;
                            } else if (tiles[x][y - 1].getValue() == null) {
                                tiles[x][y - 1].setValue(tiles[x][y].getValue());
                                tiles[x][y].setValue(null);
                                canPlace = true;
                            }
                        }
                        else{
                            gameOver = true;
                        }
                    }
                }
            }
        }
        if(canPlace){
            createNewTile(3, "up");
            moveCounter++;
            ableToMove = true;
        }
        else {
            ableToMove = false;
        }
    }

    public void moveDown() {
        boolean canPlace = false;

        for (int x = 0; x < 4; x++) {
            for (int y = 3; y >= 0; y--) {
                if (tiles[x][y].getValue() != null) {
                    if (!goingOutOfBounds("down", x, y)) {
                        if(gameOverChecker()){
                            if (mergable(x, y, 0, 1)) {
                                tiles[x][y + 1].merge(tiles[x][y]);
                                tiles[x][y].setValue(null);
                                canPlace = true;
                            }
                            if (tiles[x][y + 1].getValue() == null) {
                                tiles[x][y + 1].setValue(tiles[x][y].getValue());
                                tiles[x][y].setValue(null);
                                canPlace = true;
                            }
                        }
                        else{
                            gameOver = true;
                        }
                    }
                }
            }
        }
        if(canPlace){
            createNewTile(0, "down");
            moveCounter++;
            ableToMove = true;

        }
        else {
            ableToMove = false;
        }
    }

    private boolean goingOutOfBounds(String dir, int x, int y) {

        if (dir == "right") return x + 1 == 4;
        else if (dir == "left") return x - 1 == -1;
        else if (dir == "up") return y - 1 == -1;
        else if (dir == "down") return y + 1 == 4;

        return false;
    }

    private boolean mergable(int x, int y, int xChange, int yChange) {
        if(x + xChange < 4 && y + yChange < 4){
            if (tiles[x + xChange][y + yChange].getValue() != null && tiles[x][y].getValue() != null) {
                if (tiles[x + xChange][y + yChange].getValue() == 1 && tiles[x][y].getValue() == 2 || tiles[x + xChange][y + yChange].getValue() == 2 && tiles[x][y].getValue() == 1) {
                    return true;
                }
                return hasEqualNeighbour(x, y, xChange, yChange);
            }
        }
        return false;
    }

    private boolean hasEqualNeighbour(int x, int y, int xChange, int yChange) {
        return (tiles[x + xChange][y + yChange].getValue() == tiles[x][y].getValue()) && (tiles[x + xChange][y + yChange].getValue() != 1 && tiles[x][y].getValue() != 1) && (tiles[x + xChange][y + yChange].getValue() != 2 && tiles[x][y].getValue() != 2);
    }

    private boolean gameOverChecker(){
        boolean isNotGameOver = false;

        for (int x = 0; x < tiles[0].length; x++) {
            for (int y = 0; y < tiles.length; y++) {
                if(tiles[x][y].getValue() == null){
                    isNotGameOver = true;
                }
                else if(tiles[x][y].getValue() != null){
                    if(mergable(x ,y , 1, 0) || mergable(x ,y , 0, 1)){
                        isNotGameOver = true;
                    }

                }
            }
        }
        return isNotGameOver;
    }

    // New tiles function
    private void createNewTile(int setPosition, String dir){
        try{

            int randomPosition = utils.rng(3, 0);
            int randomValue = utils.rng(3, 1);

            if(dir == "up") {
                if(tiles[randomPosition][setPosition].getValue() == null) {
                    tiles[randomPosition][setPosition] = new Tile(randomValue);
                } else {
                    createNewTile(3, "up");
                }
            }
            else if(dir == "down") {
                if (tiles[randomPosition][setPosition].getValue() == null) {
                    tiles[randomPosition][setPosition] = new Tile(randomValue);
                } else {
                    createNewTile(0, "down");
                }
            }
            else if(dir == "right"){
                if (tiles[setPosition][randomPosition].getValue() == null) {
                    tiles[setPosition][randomPosition] = new Tile(randomValue);
                } else {
                    createNewTile(0, "right");
                }
            } else if (dir == "left") {
                if (tiles[setPosition][randomPosition].getValue() == null) {
                    tiles[setPosition][randomPosition] = new Tile(randomValue);
                } else {
                    createNewTile(3, "left");
                }
            }
        }
        catch (StackOverflowError e){
        }
    }

    public int calculateScore(){
        int totalScore = 0;

        for(Tile[] tileArray : tiles){
            for(Tile tile : tileArray){
                if(tile.getValue() != null){
                    totalScore += tile.getValue();
                }
            }
        }
        return totalScore;
    }

    // Getters
    public int getMoveCounter() { return moveCounter; }

    public boolean isGameOver() { return gameOver; }

    public boolean isAbleToMove() { return ableToMove; }

    public Tile[][] getTiles() { return tiles; }
}