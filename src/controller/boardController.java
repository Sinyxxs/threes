package controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.Board;
import model.Tile;

import javax.sound.sampled.*;
import java.io.File;
import java.net.URL;
import java.util.*;

public class boardController implements Initializable {

    private String MUSIC_FILE = "src/resources/prachtig.wav";
    @FXML
    private GridPane grid;
    @FXML
    private AnchorPane labelArea;
    @FXML
    private Label lblMove;
    @FXML
    private Label lblMoveCounter;
    private Board board;
    private Stage stage;
    private Clip clip;
    private AudioInputStream audioinput;

    public boardController(Stage stage) {
        this.stage = stage;
        board = new Board();
        music();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        createTiles();

        VBox vb = new VBox();
        vb.setAlignment(Pos.CENTER);
        vb.setPrefWidth(400);
        vb.setPadding(new Insets(30, 10, 10, 20));
        labelArea.getChildren().add(vb);


        //Move label
        lblMoveCounter = new Label("0");
        lblMoveCounter.getStyleClass().add("moveCounterLabel");
        vb.getChildren().add(lblMoveCounter);

        //Move label
        lblMove = new Label("Make a move!");
        lblMove.getStyleClass().add("moveTextLabel");
        vb.getChildren().add(lblMove);

        //Pupil label
        Label lblPupil = new Label();
        lblPupil.setText("Lorenzo Kreté");
        lblPupil.getStyleClass().add("pupilLabel");
        vb.getChildren().add(lblPupil);
    }

    private void createTiles() {
        Tile[][] tiles = board.getTiles();
        grid.getChildren().clear();
        stage.setTitle("Current score: " + board.calculateScore());

        for (int x = 0; x < tiles[0].length; x++) {
            for (int y = 0; y < tiles.length; y++) {
                drawTiles(tiles[x][y], x, y);
            }
        }
    }

    @FXML
    private void drawTiles(Tile tile, int x, int y) {
        Pane pane = new Pane();
        pane.getStyleClass().add("gamePane");

        if (tile.getValue() != null) {
            Label label = new Label();
            label.setText(tile.getValue().toString());
            label.getStyleClass().add("gamePaneNot3Label");
            // Used to set text of a pane to the center
            label.layoutXProperty().bind(pane.widthProperty().subtract(label.widthProperty()).divide(2));
            label.layoutYProperty().bind(pane.heightProperty().subtract(label.heightProperty()).divide(2));
            pane.getChildren().add(label);

            if (tile.getValue() == 1) {
                pane.getStyleClass().add("gamePane1");
            } else if (tile.getValue() == 2) {
                pane.getStyleClass().add("gamePane2");
            } else if (tile.getValue() > 2) {
                pane.getStyleClass().add("gamePane3");
                label.getStyleClass().add("gamePane3Label");
            }
        }

        grid.add(pane, x, y);
    }

    public void moveTiles(KeyCode kc) {

        if(!board.isGameOver()){
            switch (kc) {
                case UP:
                    board.moveUp();
                    editLabels("Hands UP!");
                    break;
                case DOWN:
                    board.moveDown();
                    editLabels("Going DOWN!");
                    break;
                case RIGHT:
                    board.moveRight();
                    editLabels("That's RIGHT");
                    break;
                case LEFT:
                    board.moveLeft();
                    editLabels("Moving LEFT!");
                    break;
                default:
                    break;
            }
        }
        else{
            gameOverPrompt();
        }

        createTiles();
    }

    private void gameOverPrompt(){
        restartAudio("src/resources/sad.wav");
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Gameover!");
        alert.setHeaderText("Jou score is: " + board.calculateScore());
        alert.setContentText("Wil je opnieuw spelen?");

        ButtonType button1 = new ButtonType("Ja");
        ButtonType button2 = new ButtonType("Nee");
        alert.getButtonTypes().setAll(button1, button2);

        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == button1){
            board = null;
            board = new Board();
            editLabels("Move a move!");
            restartAudio("src/resources/prachtig.wav");
        }
        else{
            stage.close();
        }
    }

    @FXML
    private void editLabels(String text) {
        lblMoveCounter.setText(Integer.toString(board.getMoveCounter()));
        if(board.isAbleToMove()){
            lblMove.setText(text);
        }
        else{
            lblMove.setText("Can't move!");
        }
    }

    private void music(){
        File musicFile = new File(MUSIC_FILE);
        try{
            if(musicFile.exists()){
                audioinput = AudioSystem.getAudioInputStream(musicFile);
                clip = AudioSystem.getClip();
                clip.open(audioinput);
                clip.start();
                clip.loop(Clip.LOOP_CONTINUOUSLY);
            }
        }
        catch (Exception ex){
            ex.printStackTrace();
        }

    }

    private void restartAudio(String musicURL){
        MUSIC_FILE = musicURL;
        try{
            audioinput.close();
            clip.close();
        }
        catch(Exception e){}
        music();
    }
}